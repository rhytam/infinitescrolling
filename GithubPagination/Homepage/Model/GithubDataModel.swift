//
//  GithubDataModel.swift
//  GithubPagination
//
//  Created by Rhytam Gulati on 02/06/21.
//

import Foundation
import RealmSwift



class GithubDataModel:Object,Decodable{
    
    @objc dynamic var id:Int = 0
    @objc dynamic var name:String?
    @objc dynamic var descriptionDetail:String?
    @objc dynamic var language:String?
    @objc dynamic var watchers_count:Int = 0
    @objc dynamic var forks:Int = 0
    
    override init() {
        super.init()
    }
    
    enum CodingKeys:String,CodingKey {
        case id,name,descriptionDetail = "description",language,watchers_count,forks
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = (try? container.decode(Int.self, forKey: .id)) ?? 0
        self.name = try? container.decode(String.self, forKey: .name)
        self.descriptionDetail = try? container.decode(String.self, forKey: .descriptionDetail)
        self.language = try? container.decode(String.self, forKey: .language)
        self.forks = (try? container.decode(Int.self, forKey: .forks)) ?? 0
        self.watchers_count = (try? container.decode(Int.self, forKey: .watchers_count)) ?? 0
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}

class MyRealmController{
    
    
    private init(){}
    
    static let shared = MyRealmController()
    
    func saveToRealm(libraries:[GithubDataModel]){
        do{
            let realm = try Realm()
            try realm.write {
                realm.add(libraries, update: .all)                
            }
        }catch let err{
            debugPrint(err.localizedDescription)
        }
    }
    
    func fetchLibrariesFromRealm(_ completion:@escaping((_ libs:Results<GithubDataModel>)->())){
        do{
            let realm = try Realm()
            completion(realm.objects(GithubDataModel.self))
        }catch let err{
            debugPrint(err.localizedDescription)
        }
    }
    
    func removeObject(){        
        fetchLibrariesFromRealm { libs in
            do{
                let realm = try Realm()
                try realm.write {
                    realm.delete(libs)
                }
            }catch let err{
                debugPrint(err.localizedDescription)
                     
            }
        }
        
    }
}
