//
//  ViewController.swift
//  GithubPagination
//
//  Created by Rhytam Gulati on 02/06/21.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let viewModel = ViewControllerModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        setupTableView()
        viewModel.initialSetup()        
        setupRefreshControl()
    }
    
    
    func setupRefreshControl(){
        let refreshControl = UIRefreshControl()
        tableView.refreshControl = refreshControl
        tableView.refreshControl?.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
    }
    
    private func setupTableView(){
        tableView.register(UINib(nibName: "GithubTableViewCell", bundle: nil), forCellReuseIdentifier: "GithubTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @objc private func pullToRefresh(){
        tableView.refreshControl?.beginRefreshing()
        viewModel.pageno = 1
        viewModel.fetchList()
    }
    
    
    
}
extension ViewController:UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.githubLibrariesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GithubTableViewCell", for: indexPath) as? GithubTableViewCell else{return UITableViewCell()}
        cell.configureCell(viewModel.githubLibrariesList?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard !viewModel.isFetching , !viewModel.hasReachedEnd, scrollView.contentSize.height >= scrollView.bounds.height else{return}
        let margin:CGFloat = 100
        if (scrollView.contentSize.height - (scrollView.contentOffset.y + scrollView.bounds.height)) < margin{
            viewModel.pageno += 1
            viewModel.fetchList()
        }
    }
    
}
extension ViewController:ViewControllerModelDelegate{
    
    func refreshTableView() {
        DispatchQueue.main.async {
            self.tableView.refreshControl?.endRefreshing()
            self.tableView.reloadData()
            
        }
        
    }
    func showSpinner(show: Bool) {
        if show{
            let spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 50))
        tableView.tableFooterView = spinner
        spinner.startAnimating()
        }else{
            if let indicator = tableView.tableFooterView as? UIActivityIndicatorView{
                indicator.stopAnimating()
            }
        }
    }
}
