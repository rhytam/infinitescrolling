//
//  GithubTableViewCell.swift
//  GithubPagination
//
//  Created by Rhytam Gulati on 02/06/21.
//

import UIKit

class GithubTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var readers: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var watchers: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var librarayImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(_ model:GithubDataModel?){
        guard let model = model else{return}
        titleLabel.text = model.name?.capitalized
        descriptionLabel.text = model.descriptionDetail
        language.text = model.language
        watchers.text = "\(model.forks)"
        readers.text = "\(model.watchers_count ?? 0)"
        
    }
}
