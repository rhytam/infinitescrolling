//
//  ViewControllerModel.swift
//  GithubPagination
//
//  Created by Rhytam Gulati on 02/06/21.
//

import Foundation
import RealmSwift

protocol ViewControllerModelDelegate:AnyObject {
    func refreshTableView()
    func showSpinner(show:Bool)
}

class ViewControllerModel{
    
    
    private var limit = 15
    
    weak var delegate:ViewControllerModelDelegate?
    var hasReachedEnd = false
    var isFetching = false
    var pageno = 1
    
    var githubLibrariesList:[GithubDataModel]?
    
    
    func initialSetup(){
        MyRealmController.shared.fetchLibrariesFromRealm { libs in
            self.githubLibrariesList = libs.map({$0})
            self.delegate?.refreshTableView()
            self.fetchList()
        }
    }
    func fetchList(){
        guard !isFetching else{return}
        let params:[String:Any] = ["per_page":limit,"page":pageno]
        delegate?.showSpinner(show: true)
        isFetching = true
        APIManager.makeGetRequest(path: "https://api.github.com/users/JakeWharton/repos", params: params) { result in            
            switch result{
            case .success(let response):
                // Will be saving only first 50 objects
                DispatchQueue.main.async {
                    if (self.githubLibrariesList?.count ?? 0) < 50{
                        MyRealmController.shared.saveToRealm(libraries: response)
                    }
                    self.githubLibrariesList?.append(contentsOf: response)
                    self.delegate?.refreshTableView()
                    self.hasReachedEnd =  response.count == 0 ? true : false
                    self.isFetching = false
                    self.delegate?.showSpinner(show: false)
                }
            case .failure(let err):
                debugPrint(err)
                self.delegate?.refreshTableView()
                self.isFetching = false
            }
        }
    }
}
