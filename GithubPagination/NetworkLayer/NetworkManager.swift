//
//  NetworkManager.swift
//  GithubPagination
//
//  Created by Rhytam Gulati on 02/06/21.
//

import Foundation

typealias resultBlock = (Result<Data,NSError>) -> ()
typealias codableBlock<T:Decodable> = (Result<T,NSError>) -> ()
extension NSError{
    static var parsingError:NSError{
        get{
            NSError(domain: "Parsing failed", code: 999, userInfo: nil)
        }
    }
}

extension Data{
    func decode<T:Decodable>(_ modelType:[T].Type) -> [T]?{
        do{
            let decoder = JSONDecoder()
            return try decoder.decode(modelType.self, from: self)
        }catch let error{
            debugPrint(error.localizedDescription)
        }
        return nil
    }
}

class APIManager{
    
    private class func decodeData<T:Decodable>(_ response:Result<Data,NSError>,modelType:[T].Type,completion: (Result<[T],NSError>) -> ()){
        switch response{
        case .success(let data):
            guard let parsedInfo = data.decode(modelType) else{completion(.failure(NSError.parsingError)); return }
            completion(.success(parsedInfo))
        case .failure(let err):
            completion(.failure(err))
        }
    }
    
    
    class func makeGetRequest(path:String,params:[String:Any],completion:@escaping(codableBlock<[GithubDataModel]>)){
        guard let url = URL(string: path) else{return}
        var request = URLRequest(url: url)
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) {
            urlComponents.queryItems = [URLQueryItem]()
            for (key,value) in params {
                let queryItem = URLQueryItem(name: key, value: "\(value)")
                urlComponents.queryItems?.append(queryItem)
            }
            request.url = urlComponents.url
            debugPrint("Request URL =========  \(request.url)")
            NetworkManager.hitRequest(request: request) { result in
                decodeData(result, modelType: [GithubDataModel].self, completion: completion)
            }
        }
    }
}

class NetworkManager{
    class func hitRequest(request: URLRequest, completion:@escaping (Result<Data,NSError>) -> ()) {
                                           
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let data = data, error == nil{
                completion(.success(data))
            }else{
                completion(.failure(error! as NSError))
            }
        }.resume()
    }
    
   
}
